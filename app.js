const express=require('express');
const mongoose=require('mongoose');
const bodyParser=require('body-parser');
const userRoutes = require('./routes');
const app = express();


app.use(bodyParser.json());
app.use('/user', userRoutes);

mongoose
  .connect(
    'mongodb+srv://Varun:8wct9xtkAzNwZOJ4@cluster0.whpqx.mongodb.net/ExpensePlanner?retryWrites=true&w=majority'
  )
  .then(result => {
      console.log('Connected');
    app.listen(3000);
  })
  .catch(err => console.log(err));
