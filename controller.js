const User = require("./model");


exports.createTransaction = (req, res, next) => {
  
  const {title,amount,date} = req.body;
  
      const user = new User({
        title: title,
        amount: amount,
        date: date
      });
    user.save()
    .then(result => {
        res.status(201).json({ message: 'Transaction created!', expenseId: result._id });
      })
      .catch(err => {
        if (!err.statusCode) {
          err.statusCode = 500;
        }
        next(err);
      });
  };

//   exports.getTransactions=(req, res, next)=>{
//     const transactions =  User.find()
// 	res.status(200).json(transactions)
//   }
//   exports.login = (req, res, next) => {
//     console.log(req.body.email,req.body.pwd);
//     const email = req.body.email;
//     const pwd = req.body.pwd;
    
//     User.findOne({ email: email,pwd:pwd})
//       .then(user => {
//         if (!user) {
//           const error = new Error('User not found.');
//           error.statusCode = 401;
//           throw error;
//         }
        
      
//         res.status(200).json({user:user });
//       })
//       .catch(err => {
//         if (!err.statusCode) {
//           err.statusCode = 500;
//         }
//         next(err);
//       });
//   };


//   exports.updateUser = (req, res, next) => {
    
//     const errors = validationResult(req);
//     if (!errors.isEmpty()) {
//       const error = new Error('Validation failed, entered data is incorrect.');
//       error.statusCode = 422;
//       throw error;
//     }
//     const uid=req.body._id;
//     const fname = req.body.first_name;
//   const lname = req.body.surname;
//   const age = req.body.age;
//   const adhaar = req.body.adhaar;
//   const email = req.body.email;
//   const pass = req.body.pwd;
    
//     User.findById(uid)
//       .then(user => {
//         if (!user) {
//           const error = new Error('Could not find user.');
//           error.statusCode = 404;
//           throw error;
//         }
        
//         user.first_name = fname;
//         user.surname = lname;
//         user.age = age;
//         user.adhaar=adhaar;
//         user.email=email;
//         user.pwd=pass;
//         return user.save();
//       })
//       .then(result => {
//         res.status(200).json({ message: 'User updated!', user: result });
//       })
//       .catch(err => {
//         if (!err.statusCode) {
//           err.statusCode = 500;
//         }
//         next(err);
//       });
//   };
