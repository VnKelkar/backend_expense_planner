const express = require('express');


const User = require('./model');
const userController = require('./controller');

const router = express.Router();

router.post('/create-transaction', async(req,res) => {
    const data = req.body
    const result =  User.create(data);
    res.send({data});
    console.log(data);
});

router.get("/transactions", async (req, res, next) => {
    console.log('Hit');
	const transaction = await User.find()
	res.send(transaction)
})

//router.post('/update',userController.updateUser);

module.exports=router;
